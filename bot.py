import logging
import os
from datetime import datetime
from enum import Enum

import dotenv
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, Updater

dotenv.load_dotenv()

logging.basicConfig(
    level=logging.INFO,
    style="{",
    format="{name} -- [{levelname}] {asctime}: {message}",
)

TELEGRAM_API_TOKEN = os.getenv("TELEGRAM_API_TOKEN")
WEBHOOK_URL = os.getenv("WEBHOOK_URL")
PORT = int(os.getenv("PORT", "8443"))


class DiesDeLaSetmana(Enum):
    dilluns = "1"
    dimarts = "2"
    dimecres = "3"
    dijous = "4"
    divendres = "5"
    dissabte = "6"
    diumenge = "7"


def quin_dia_es_avui() -> str:
    return datetime.today().strftime("%w")


def is_day_of_the_week(dia_de_la_setmana: DiesDeLaSetmana) -> bool:
    return quin_dia_es_avui() == dia_de_la_setmana.value


def divendres(update: Update, context: CallbackContext) -> None:
    message = (
        ("És divendres!!!! 🤩🎉🤠👯‍♀💃🙌😏🥳")
        if is_day_of_the_week(DiesDeLaSetmana.divendres)
        else "No, encara no és divendres 🥺😭😩😫😢"
    )

    update.message.reply_text(message)


def start_bot():
    updater = Updater(TELEGRAM_API_TOKEN)
    updater.dispatcher.add_handler(CommandHandler("divendres", divendres))

    if os.getenv("ENV"):
        updater.start_webhook(
            listen="0.0.0.0",
            port=PORT,
            url_path=TELEGRAM_API_TOKEN,
            webhook_url=f"{WEBHOOK_URL}/{TELEGRAM_API_TOKEN}",
        )
    else:
        logging.info("Starting in local polling mdoe")
        updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    start_bot()
